import { Col, Container, Row } from "react-bootstrap";
import styles from "./home.module.scss"
import user from "../../assets/user.jpg"
import logo from "../../assets/banner/logo2.png"
import { IconButton } from "../../components/buttons";
import arrow from "../../assets/icons/arrow.svg"
import share from "../../assets/icons/share.svg"
import { AppMap } from "../../components/map";
import { AppSlider } from "../../components/rangeSlider";
import { AccuracyCard } from "../../components/common";
import { AppGraph } from "../../components/graph";
import { AppHeader } from "../../components/header";
import { RangeSlider } from "../../components/slider";


function Home() {
  
  return (
    <div className={styles.home_main}>
      <AppHeader />
      <div className={styles.header}>
        <Container>
          <Row className={styles.header_row}>
            <Col sm={12} md={4} >
              <div className={styles.user_header}>
                <p className={styles.title}>Prepared by paul Chapman</p>
                <div className={styles.logo_div}>
                  <div className={styles.user_img}>
                    <img src={user} />
                  </div>
                  <img className={styles.logo} src={logo} />
                </div>
                <p className={styles.map_title}>
                  Hodford Road <br /> London, NW11 8EG
                </p>
              </div>
            </Col>
            <Col className="d-none d-md-block" sm={12} md={4}>
              <div className={styles.bbtn_div}>
                <IconButton title="Download" height="36px" margin="0 15px" width="130px" iconLeft={arrow} imgWidth="16px" imgHeight="16px" />
                <IconButton title="Share" height="36px" width="130px" iconLeft={share} imgWidth="16px" imgHeight="16px" />
              </div>
            </Col>
          </Row>
        </Container>
        <div className={styles.map_sec}>
          <AppMap />
        </div>
        <Container className={styles.rang_slider}>
          <AppSlider />
        </Container>
      </div>
      <div className={styles.graph_sec}>
        <Container>
          <Row className={styles.row}>
            <Col className={styles.col} xs={12} md={5}>
              <div>
                <AccuracyCard />
              </div>
            </Col>
            <Col className={styles.col} xs={12} md={7}>
              <AppGraph />
              
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}

export default Home;
