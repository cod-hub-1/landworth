import logo from './logo.svg';
import './App.css';
import AppRouter from './components/navigation';

function App() {
  return (
    <AppRouter/>
  );
}

export default App;
