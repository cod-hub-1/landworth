import React, { useState } from 'react'
import { Button, Tabs, Tab } from 'react-bootstrap'
import styles from "./common.module.css"
import info from "../../assets/icons/info.png"
import speed from "../../assets/icons/speed.png"
import houses from "../../assets/icons/houses.png"
import { BsInfoCircle } from "react-icons/bs";

export const AccuracyCard = () => {
    return (
        <div>
            <div className={styles.accuracy_card}>
                <p className={styles.acrcy_crd_title}>£000,000 <span className={styles.acrcy_crd_heghtlight}>£00/sqft</span></p>
                <div className={styles.acrcy_crd_row}>
                    <p className={styles.crd_title}>As of January 01 2021 </p>
                    <div className={styles.icons_div}>
                        <p className={styles.head}>Accuracy</p>
                        <div className={styles.spd_div} >
                            <img className={styles.spd} src={speed} />
                        </div>
                        <div className={styles.info_div} >
                            <BsInfoCircle className={styles.info} />
                        </div>
                    </div>
                </div>
                <p className={styles.desc}>Source: Lorem ipsum dolor sit amet, consectetur adipisici</p>
            </div>
            <AccuracyTabs />
        </div>
    )
}
export const AccuracyTabs = () => {
    const [key, setKey] = useState('specs');
    return (
        <div className="acrcy_tabs">
            <Tabs
                id="controlled-tab-example"
                activeKey={key}
                onSelect={(k) => setKey(k)}

            >
                <Tab eventKey="specs" title="Specs">
                    <div className={styles.tab_div}>
                        <TabList {...{
                            title: 'Type',
                            desc: 'House'
                        }}
                        />
                        <TabList
                            {...{
                                title: 'Internal Floor Area',
                                desc: '00 m2'
                            }}
                        />
                        <TabList
                            {...{
                                title: 'Plot Size',
                                desc: '00 m2'
                            }}
                        />
                        <TabList
                            {...{
                                title: 'Bedrooms',
                                desc: '04'
                            }}
                        />
                    </div>
                </Tab>
                <Tab eventKey="history" title="History">
                    <div className={styles.tab_div}>
                        <TabList {...{
                            title: 'Type',
                            desc: 'House'
                        }}
                        />
                        <TabList
                            {...{
                                title: 'Internal Floor Area',
                                desc: '00 m2'
                            }}
                        />

                    </div>
                </Tab>
                <Tab eventKey="floor" title="Floor Plan">
                    <div className={styles.tab_div}>
                        <TabList {...{
                            title: 'Type',
                            desc: 'House'
                        }}
                        />
                        <TabList
                            {...{
                                title: 'Internal Floor Area',
                                desc: '00 m2'
                            }}
                        />
                        <TabList
                            {...{
                                title: 'Plot Size',
                                desc: '00 m2'
                            }}
                        />

                    </div>
                </Tab>
                <Tab eventKey="tags" title="Tags">
                    <div className={styles.tab_div}>
                        <TabList {...{
                            title: 'Type',
                            desc: 'House'
                        }}
                        />

                    </div>
                </Tab>
            </Tabs>
        </div>
    )
}
export const TabList = ({ title, desc }) => {
    return (
        <div className={styles.tab_list}>
            <div className={styles.icon_div}>
                <img src={houses} />
                <p className={styles.title}>{title}</p>
            </div>
            <p className={styles.list_desc}>{desc}</p>
        </div>
    )
}

