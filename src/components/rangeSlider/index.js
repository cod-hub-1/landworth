import React, { useState } from 'react';
import { Col, Container, Row } from "react-bootstrap";
import styles from "./slider.module.css"
import { IconButton } from '../buttons';
import { RangeSlider } from '../slider';
import { useMediaQuery } from 'react-responsive'
import classNames from 'classnames';

export const AppSlider = () => {
    const isMobileScreen = useMediaQuery({ query: '(min-width: 567px)' })
    const [value, setValue] = useState(0);

    return (
        <Row className={styles.slider_div}>
            <Col sm={12} md={3} lg={2} className={styles.slide_col} >
                <p className={styles.title}>Select Property Condition</p>
            </Col>
            <Col sm={12} md={6} lg={7} className={styles.slider}>
                <Row>
                    <Col style={{padding: 0}} md={{ span: 12, order: 2 }} >
                        <RangeSlider />
                    </Col>
                    <Col style={{padding: 0}} md={{ span: 12, order: 1 }} >
                        <Row className={styles.slider_row}>
                            <div className={styles.slider_content_div}>
                                <div className={styles.dot} />
                                <div className={styles.text_div}>
                                    <p className={styles.heading}>Low {isMobileScreen && 'End'}  £000,000</p>
                                    <p className={styles.heighlight}>£00/sqft</p>
                                </div>
                            </div>
                            <div className={styles.slider_content_div}>
                                <div className={styles.dot} style={{ backgroundColor: '#56A2CA' }} />
                                <div className={styles.text_div}>
                                    <p className={styles.heading}>Average £000,000</p>
                                    <p className={styles.heighlight}>£00/sqft</p>
                                </div>
                            </div>
                            <div className={styles.slider_content_div}>
                                <div className={styles.dot} style={{ backgroundColor: '#1F1243' }} />
                                <div className={styles.text_div}>
                                    <p className={styles.heading}>High {isMobileScreen && 'End'}£000,000</p>
                                    <p className={styles.heighlight}>£00/sqft</p>
                                </div>
                            </div>
                        </Row>
                    </Col>
                </Row>
            </Col>
            <Col sm={12} md={3} lg={2} className={classNames(styles.slide_col, styles.bttn_col)}>
                <IconButton title="Confirm" className={styles.slider_bttn} />
            </Col>
        </Row>
    );

};
