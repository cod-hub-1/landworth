import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Home from "../../pages/home";

export default function AppRouter() {
  return (
    <Router>
        <Switch>
          <Route path="/" component={Home} />
        </Switch>
    </Router>
  );
}
