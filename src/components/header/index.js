import { Navbar, Container, Nav, } from "react-bootstrap"
import { FaSignOutAlt } from "react-icons/fa"
import logo from "../../assets/icons/logo.png"


// css in index.css

export const AppHeader = () => {
    return (
        <div className="menu_div">
            <Navbar  expand="lg">
                <Container>
                    <Navbar.Brand href="#home">
                        <img src={logo} />
                        LANDWORTH
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="">
                            <Nav.Link href="#map">Map</Nav.Link>
                            <Nav.Link href="#search">Search</Nav.Link>
                            <Nav.Link href="#search">Valuation</Nav.Link>
                        </Nav>
                        <div className="nav-signout-div">
                            <p>Sign out</p>
                            <FaSignOutAlt className="signout-icon" />
                        </div>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
    )
}

