import classNames from 'classnames'
import { Button } from 'react-bootstrap'
import styles from "./button.module.scss"

export const IconButton = ({
  height = '60px',
  width = '100%',
  backgroundColor = "#fff",
  color = "#210C4C",
  borderRadius = '2px',
  fontSize = '12px',
  title,
  iconLeft = null,
  iconRight = null,
  imgHeight,
  imgWidth,
  margin = '0px',
  onclick,
  marginRight =" 10px",
  className

}) => {
  return (
    <Button
      onClick={onclick}
      style={{
        height,
        width,
        backgroundColor,
        borderRadius,
        margin,
        fontSize,
        color,
      }}
      className={classNames(styles.icon_bttn, className)}
    >
      {iconLeft && <img height={imgHeight} width={imgWidth} src={iconLeft} style={{marginRight}} />}
      {
        title
      }{iconRight && <img height={imgHeight} width={imgWidth} src={iconRight}  style={{marginLeft:10}} />}

    </Button>
  )
}

