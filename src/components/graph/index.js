import classNames from 'classnames'
import { useState } from 'react';
import { Button } from 'react-bootstrap'
import styles from "./graph.module.css"
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
  } from 'chart.js';
  import { Line } from 'react-chartjs-2';
  import faker from 'faker';



export const AppGraph = () => {
    ChartJS.register(
        CategoryScale,
        LinearScale,
        PointElement,
        LineElement,
        Title,
        Tooltip,
        Legend
      );
      const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
      const options = {
        responsive: true,
        plugins: {
          legend: {
              display: false,
        
          },
          title: {
            display: false,
          },
          tooltip:{
            enabled: false,
          }
        },
      };
       const data = {
        labels,
        datasets: [
          {
            pointRadius: 0,
            label: 'Dataset 1',
            data: labels.map(() => faker.datatype.number({ min: 0, max: 1000000 })),
            borderColor: 'rgb(255, 99, 132)',
            backgroundColor: 'rgba(255, 99, 132, 0.5)',
          },
          {
            pointRadius: 0,
            label: 'Dataset 2',
            data: labels.map(() => faker.datatype.number({ min: 0, max: 1000000 })),
            borderColor: 'rgb(53, 162, 235)',
            backgroundColor: 'rgba(53, 162, 235, 0.5)',
          },
        ],
      };

    return (
        <div className={styles.graph_div}>
            <p className={styles.title}>Property Timeline</p>
            <div className={styles.graph_label}>
                <p>Low</p>
                <p>Average</p>
                <p>Heigh</p>
            </div>
            <div>
            <Line options={options} data={data} />
            </div>
        </div>
    )
}

