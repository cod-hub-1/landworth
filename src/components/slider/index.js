import React from "react"
import styles from "./slider.module.css"

const genSlideStyle = (value) => {
    return {
        point: {
            left: `calc(${value * 50}% - ${1 + 12 * value}px)`,
        },
        range: {
            width: `${value * 50}%`,
        },
    };
};

export class RangeSlider extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 1,
        }
    }

    handleChange = (e) => {
        this.setState({ value: e.target.value });
    }

    render() {
        const slideStyle = genSlideStyle(this.state.value);
        return (
            <div className={styles.range}>
                <span className={styles.bullet} style={{right: '50%'}} />
                <span className={styles.range_value} style={slideStyle.range} />
                <span className={styles.circle} style={slideStyle.point} />
                <span className={styles.circle_bg} style={slideStyle.point} />
                <input
                    className={styles.range_slide}
                    name="range"
                    type="range"
                    min="0"
                    max="2"
                    value={this.state.value}
                    // step="1"
                    onChange={this.handleChange}
                />
            </div>
        );
    }
}
