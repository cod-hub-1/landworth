import { Col, Container, Row } from "react-bootstrap";
import ReactMapboxGl, { Layer, Feature } from "react-mapbox-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import styles from "./map.module.css";
import home from "../../assets/banner/home.png";

export function AppMap() {
  const Map = ReactMapboxGl({
    accessToken:
      "pk.eyJ1IjoicHJjaGFwbWFuIiwiYSI6ImNreGVzdnFndTFibjAycnBkeGk5ZDkyY28ifQ.298vwqbLP8AZNhr20TrPpw"
  });
  return (
    <div className={styles.map_div}>
      <Map
        className={styles.map}
        style="mapbox://styles/mapbox/streets-v9"
        containerStyle={{
          height: "360px",
          width: "100%"
        }}
      >
        <Layer type="symbol" id="marker" layout={{ "icon-image": "marker-15" }}>
          <Feature coordinates={[-0.481747846041145, 51.3233379650232]} />
        </Layer>
      </Map>
      <div className={styles.map_text_div}>
        <div className={styles.text_background}></div>
        <div className={styles.content}>
          <p className={styles.title}>
            Hodford Road
            <br /> London, NW11 8EG
          </p>
        </div>
      </div>
      <div className={styles.map_img}>
        <img src={home} />
      </div>
    </div>
  );
}
